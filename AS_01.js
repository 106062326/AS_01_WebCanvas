var canvas = document.getElementById("myCanvas");
var ctx = canvas.getContext("2d");


var choose_color = "#000000";
var font_size = 20;
var brush_size = 10;
var brush_size_defaut = 1;
var font_style = "Arial";
var tool;
var isdrawing;
var start_pos={x:0 , y:0};
var last_pos={x:0 , y:0};
var fix_pos={x:450, y:50};
var now_pos={x:0 , y:0};
var text_value;
var text_flag = 0;
var image_data1=[];
var image_data2=[];
var self_image;
var circle_radius = 0;
var tmp_data;
var solid= false;
var brush_slider;
var btn1, btn2, btn3, btn4, btn5, btn7;
document.addEventListener("mousemove", move);
document.addEventListener("mousedown", down);
document.addEventListener("mouseup", up);
function initial(){
    canvas = document.getElementById("myCanvas");
    ctx = canvas.getContext("2d");
    document.addEventListener("mousemove", move);
    document.addEventListener("mousedown", down);
    document.addEventListener("mouseup", up);
    //$('#text_form').hide();
    ctx.lineWidth = brush_size;
    ctx.strokeStyle = choose_color;
    ctx.font = font_size +"px "+ font_style;
    brush_slider = document.getElementById("brush_size_chooser");
    $('#text_form').hide();
    
}

function previewFile(){
    self_image = document.querySelector('.upload'); 
    var file    = document.querySelector('input[type=file]').files[0]; //sames as here
    var reader  = new FileReader();

    reader.onloadend = function () {
        self_image.src = reader.result;
    }

    if (file) {
        reader.readAsDataURL(file); //reads the data as a URL
    } else {
        self_image.src = "";
    }
    //set_tool("draw_self");
}

function set_tool(input){
    
    tool = input;
    
    $('#text_form').hide()
    switch(tool){
        case "pen":
            ctx.strokeStyle = choose_color;
            ctx.lineWidth = brush_size;
            canvas.setAttribute("style", "cursor:url(image/Handwriting.cur),default;");
            break;
        case "eraser":
            ctx.strokeStyle = "#FFFFFF";
            canvas.setAttribute("style", "cursor:url(image/eraser.cur),default;");
            ctx.lineWidth = brush_size;
            break; 
        case "line":
            save();
            ctx.lineWidth = brush_size;
            canvas.style.cursor="crosshair";
        break;
        case "circle":
            save();
            canvas.style.cursor="crosshair";
            ctx.lineWidth= brush_size_defaut;
            break;
        case "triangle":
            save();
            canvas.style.cursor="crosshair";
            ctx.lineWidth = brush_size_defaut;
            break;
        case "rectangle": 
            canvas.style.cursor="crosshair";
            ctx.lineWidth = brush_size_defaut;
            break; 
        case "text":
            document.getElementById("text_input").value =""; 
            $('#text_form').show()
            canvas.style.cursor="text"
            text_flag = 0;
            break;  
        case "draw_self":
            self_image = document.querySelector('.upload');
            break;

    }
}
function set_solid(){
    $('#text_form').hide()
    solid = !solid;
    btn = document.getElementById("solid");
    if(solid) btn.style.opacity="1";
    else btn.style.opacity="0.8"
}
function set_brush_size(){
    brush_size = brush_slider.value;
    console.log("brush_size:"+brush_size);
    ctx.lineWidth = brush_size;
}
function set_font(element){
    var choose1 = document.getElementById("font_size_chooser");
    var choose2 = document.getElementById("font_style_chooser");
    font_size = choose1.options[choose1.selectedIndex].value;
    font_style = choose2.options[choose2.selectedIndex].value;
    element.style.fontFamily = choose2.options[choose2.selectedIndex].value;
    console.log("font_size:"+font_size);
    ctx.font = font_size +"px "+ font_style;
    console.log(ctx.font);
}
$(document).ready(function() {
    $('#colorpicker').farbtastic('#color');
    
  });
$('#colorpicker').farbtastic(function(color) {
    
    choose_color = color;
    ctx.strokeStyle = choose_color;
    ctx.fillStyle = choose_color;
});
$('#text_input').change(function(){
    text_value =$('#text_input').val();
    //console.log($('#text_input').val());
    text_flag=1;
})
function submit_text(){
    //console.log($('text_input').value);
    text_value=$('text_input').value;
    text_flag = 1;
}
function down(e){
    now_pos.x = e.offsetX;
    now_pos.y = e.offsetY;
    if(e.clientX-fix_pos.x>=100 && e.clientX-fix_pos.x<=canvas.width && e.clientY-fix_pos.y>=100 &&e.clientY-fix_pos.y<=canvas.height){
        image_data1.push(ctx.getImageData(0,0,canvas.width, canvas.height));
        console.log("save_data1"+image_data1);
    }
   
    
    isdrawing = true;
    switch(tool){
        case "pen":
            ctx.moveTo(e.offsetX, e.offsetY);
            ctx.beginPath();
            ctx.strokeStyle = choose_color;
            break;
        case "eraser":
            ctx.moveTo(e.offsetX, e.offsetY);
            ctx.beginPath();
            ctx.strokeStyle = "#FFFFFF";
            break;
        case "line":
            start_pos.x = e.offsetX;
            start_pos.y = e.offsetY;
            ctx.strokeStyle = choose_color;
            break;
        case "circle":
            last_pos.x = e.offsetX;
            last_pos.y = e.offsetY;
            ctx.strokeStyle = choose_color;
            ctx.fillStyle = choose_color;
            circle_radius = 0;
            break;
        case "triangle":
            last_pos.x = e.offsetX;
            last_pos.y = e.offsetY;
            ctx.strokeStyle = choose_color;
            ctx.fillStyle = choose_color;
            break;
        case "rectangle":
            last_pos.x = e.offsetX;
            last_pos.y = e.offsetY;
            ctx.strokeStyle = choose_color;
            ctx.fillStyle = choose_color;
            break;
        case"text":
            if(text_flag)
            {
                ctx.fillText(text_value, e.clientX-fix_pos.x, e.clientY-fix_pos.y); 
            }
            break;
        case "draw_self":
            last_pos.x = e.offsetX;
            last_pos.y = e.offsetY;
            rec_x=0;
            rec_y=0;
            break;

    }
    
}

function move(e){
    if(isdrawing && e.clientX-fix_pos.x>=0 && e.clientX-fix_pos.x<=canvas.width && e.clientY-fix_pos.y>=0 &&e.clientY-fix_pos.y<=canvas.height){
        switch(tool){
            case "pen":
                ctx.lineTo(e.offsetX, e.offsetY);
                ctx.stroke();
                break;
            case "eraser":
                ctx.lineTo(e.offsetX, e.offsetY);
                ctx.stroke();
                break;
            case "line":
                load().then((mes)=>{
                    console.log(mes);
                    ctx.beginPath();
                    ctx.moveTo(start_pos.x, start_pos.y);
                    ctx.lineTo(e.offsetX, e.offsetY);
                    ctx.stroke();
                    ctx.closePath();
                });
                break;
            case "circle":
                load().then((mes)=>{
                    console.log(mes);
                    ctx.beginPath();
                    circle_radius = Math.abs(last_pos.x-e.offsetX);
                    ctx.arc(last_pos.x, last_pos.y,circle_radius,0,2*Math.PI);
                    ctx.stroke();
                    if(solid) ctx.fill();
                    ctx.closePath();
                });
                
                break;
            case "triangle":
                load().then((mes)=>{
                    console.log(mes);
                    ctx.beginPath();
                    var rx = (e.offsetX-last_pos.x)/2;
                    var ry = e.offsetY-last_pos.y;
                    ctx.moveTo(last_pos.x, last_pos.y);
                    ctx.lineTo(last_pos.x+2*rx, last_pos.y);
                    ctx.lineTo(last_pos.x+rx,last_pos.y+ry);
                    ctx.lineTo(last_pos.x, last_pos.y);
                    ctx.stroke();
                    if(solid) ctx.fill();
                    ctx.closePath();
                    //ctx.fill();
                });
                break;
            case "rectangle":
                load().then((mes)=>{
                    console.log(mes);
                    ctx.beginPath();
                    var rx = e.offsetX-last_pos.x;
                    var ry = e.offsetY-last_pos.y;
                    ctx.moveTo(last_pos.x, last_pos.y);
                    ctx.lineTo(last_pos.x+rx, last_pos.y);
                    ctx.lineTo(last_pos.x+rx,last_pos.y+ry);
                    ctx.lineTo(last_pos.x, last_pos.y+ry);
                    ctx.lineTo(last_pos.x, last_pos.y);
                    ctx.stroke();
                    if(solid) ctx.fill();
                    ctx.closePath();
                });
                break;
            case "text":
                break;
            case "draw_self":
                load().then((mes)=>{
                    console.log(mes);
                    ctx.beginPath();
                    var rx = e.offsetX-last_pos.x;
                    var ry = e.offsetY-last_pos.y;
                    ctx.drawImage(self_image,last_pos.x,last_pos.y,rx,ry);
                    ctx.closePath();
                });
                break;
        }
    }
}
function up(e){
    
    if(isdrawing){
        ctx.closePath();
        isdrawing = false;
        if(e.clientX-fix_pos.x>=0 && e.clientX-fix_pos.x<=canvas.width && e.clientY-fix_pos.y>=0 &&e.clientY-fix_pos.y<=canvas.height){
            image_data2.push(ctx.getImageData(0,0,canvas.width, canvas.height));
            console.log("save_data2"+image_data2);
        }
        save();

    }
    
}
function undo(){
    //console.log("undo");
    image_data2.push(ctx.getImageData(0,0,canvas.width, canvas.height));
    ctx.putImageData(image_data1[image_data1.length-1],0,0);

    image_data1.pop();
    
}
function redo(){
    //console.log("redo");
    image_data1.push(ctx.getImageData(0,0,canvas.width, canvas.height));
    ctx.putImageData(image_data2[image_data2.length-1], 0, 0);
    image_data2.pop();

}
function clean_up(){
    //console.log("clear");
    
    image_data1.push(ctx.getImageData(0,0,canvas.width, canvas.height));
    ctx.clearRect(0,0,canvas.width, canvas.height);
}
function download(){
    var download = document.getElementById("download");
    var image = canvas.toDataURL("image/png");
    //console.log("image:"+image);
    download.setAttribute("href", image);
}
function load(){
    //console.log("load");
    return new Promise((resolve, reject) => {
        var imageObj = new Image();
        imageObj.onload = function () {
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        ctx.drawImage(this, 0, 0);
        resolve("load done!");
        };
        imageObj.src = tmp_data;
      });
    
}
function save(){
   //console.log("save");
    tmp_data=canvas.toDataURL();
}
function reset_btn(){

}
