# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

| **Item**                                         | **Score** |
| :----------------------------------------------: | :-------: |
| Basic components                                 | 60%       |
| Advance tools                                    | 35%       |
| Appearance (subjective)                          | 5%        |
| Other useful widgets (**describe on README.md**) | 1~10%     |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

---

## Put your report below here
**Basic component:**
   * Basic control tools (30%):
        -Brush and eraser:按下按鈕後就可以在canvas上作畫及擦掉
        -Color selector: color picker在畫面左上角
        -Brush size: 在color picker下方，透過slider調整畫筆大小
   * Text input (10%):
        -按下文字按鈕(T)後，會出現可填入文字的回答框，打好後再canvas上按左鍵，就會出現文字了
        -後方可選擇字體大小與字型
        -一旦按下其他的按紐，輸入文字的回答框會消失
*   Cursor icon (10%)
        -移動到按鈕上時會改變cursor為"pointer"圖案
        -點選筆，cursor會變成筆形狀，選橡皮擦會變成橡皮擦形狀
        -拉直線、三角形、圓形、四角形時，cursor會是十字形狀
        -text狀態會是"text"圖案
*   Refresh button (10%)
        -按鈕第三列最右邊，掃把圖案的按鈕為refresh button，會清空畫布

**Advance tools**  
*   Different brush shapes (15%)
        -Straight line, circle, rectangle and triangle 在按鈕第二列
        -可以透過第一列最左邊的按紐，調整為圖案為實心填滿還是空心
        (按鈕為半透明時畫出來的圖形為空心) 
*   Un/Re-do button (10%)
        -第三列前兩個按鈕分別為undo redo，可向前回朔好幾步
*   Image tool (5%)
        -第四列有個選擇檔案的按紐，按下後可上傳自己的圖片
        -上傳完後圖片會顯示在旁邊，點選該圖片後，就能像拉四角形的方式，將該圖片加入畫布任何地方
*   Download (5%)
        -第四列第一個按鈕為下載鍵，可將畫布上的圖片下載下來

**Other useful widgets(1~10%)**
    -大概就那個實心空心按紐吧
    -怕我的圖其實不夠直覺，附上左側工具箱的各按鈕內容
    ----------------------------------------
    | pen  | eraser | text | solid control |
    ----------------------------------------
    | line | circle | triangle | rectangle |
    ----------------------------------------
    | undo | redo | refresh|
    ---------------------------------------------------
    | download | upload image |upload button|
    ---------------------------------------------------




